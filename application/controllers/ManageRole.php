<?php
    Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
    Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
    Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
class ManageRole extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('ManageRole_model');
    }
    public function insertDetails()
	{
		$response = $this->ManageRole_model->insertDetails();
		if($response ==1){
			$userdata['status'] = array('status' => "1", "message" => "Role added successfully.");
			$userdata['data'] = $response;
        }else if($response ==-1){
			$userdata['status'] = array('status' => "0", "message" => "Role name already saved.");
			
		}else{
			$userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}
    
    public function getDetails()
    {
		$userList = $this->ManageRole_model->getDetails();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
    }
    
	public function onChangeSearch()
    {
		$userList = $this->ManageRole_model->onChangeSearch();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
	}
	
	public function updateDetails()
	{
        $response = $this->ManageRole_model->updateDetails();
        if($response == 1){
                $Formsdata['status'] = array('status' => "1", "message" => "Role details updated successfully.");
                $Formsdata['data'] = $response;
        }else if($response == -1){
                $Formsdata['status'] = array('status' => "0", "message" => "Nothing to update.");
        }else{
                $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
 
	public function deleteRole()
	{
		$roleId = $this->input->post('roleId');
		$user = $this->ManageRole_model->deleteRole($roleId);
        $response = array(
            'message' => 'Role name deleted successfully.'
        );
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }
    
    public function getAccessPermissions()
    {
		$userList = $this->ManageRole_model->getAccessPermissions();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
	}

    
}

