<?php
    Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
    Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
    Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
class ManageAccess extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('ManageAccess_model');
    }
    public function insertDetails()
	{
		$response = $this->ManageAccess_model->insertDetails();
		if($response ==1){
			$userdata['status'] = array('status' => "1", "message" => "Access details added successfully.");
			$userdata['data'] = $response;
        }else if($response ==-1){
			$userdata['status'] = array('status' => "0", "message" => "Access details already saved.");
			
		}else{
			$userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}
    
    public function getDetails()
    {
		$userList = $this->ManageAccess_model->getDetails();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
    }

    public function getUserDeatils()
    {
		$userList = $this->ManageAccess_model->getUserDeatils();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
    }
    
	public function onChangeSearch()
    {
		$userList = $this->ManageAccess_model->onChangeSearch();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
	}
	
	public function updateDetails()
	{
        $response = $this->ManageAccess_model->updateDetails();
        if($response == 1){
                $Formsdata['status'] = array('status' => "1", "message" => "Access details updated successfully.");
                $Formsdata['data'] = $response;
        }else if($response == -1){
                $Formsdata['status'] = array('status' => "0", "message" => "Nothing to update.");
        }else{
                $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
 
	public function deleteDetails()
	{
		$accessId = $this->input->post('accessId');
		$user = $this->ManageAccess_model->deleteDetails($accessId);
        $response = array(
            'message' => 'Access details deleted successfully.'
        );
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

    public function getAccessPermissions()
    {
		$userList = $this->ManageAccess_model->getAccessPermissions();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
	}
}

