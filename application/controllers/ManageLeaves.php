<?php
    Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
    Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
    Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
class ManageLeaves extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('ManageLeaves_model');
    }
    public function insertDetails()
	{
		$response = $this->ManageLeaves_model->insertDetails();
		if($response ==1){
			$userdata['status'] = array('status' => "1", "message" => "Leaves Details added successfully.");
			$userdata['data'] = $response;
        }else if($response ==-1){
			$userdata['status'] = array('status' => "0", "message" => "Leaves Details already saved.");
			
		}else{
			$userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}
    
    public function getDetails()
    {
		$userList = $this->ManageLeaves_model->getDetails();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
    }

    public function getUserDeatils()
    {
		$userList = $this->ManageLeaves_model->getUserDeatils();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
    }
    
	public function onChangeSearch()
    {
		$userList = $this->ManageLeaves_model->onChangeSearch();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
	}
	
	public function updateDetails()
	{
        $response = $this->ManageLeaves_model->updateDetails();
        if($response == 1){
                $Formsdata['status'] = array('status' => "1", "message" => "Leaves updated successfully.");
                $Formsdata['data'] = $response;
        }else if($response == -1){
                $Formsdata['status'] = array('status' => "0", "message" => "Nothing to update.");
        }else{
                $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
 
	public function deleteDetails()
	{
		$leaveId = $this->input->post('leaveId');
		$user = $this->ManageLeaves_model->deleteDetails($leaveId);
        $response = array(
            'message' => 'leaves Details deleted successfully.'
        );
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

    public function getAccessPermissions()
    {
		$userList = $this->ManageLeaves_model->getAccessPermissions();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
	}
    
}

