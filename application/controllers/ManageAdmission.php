<?php
    Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
    Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
    Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
class ManageAdmission extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('ManageAdmission_model');
    }
    public function insertDetails()
	{
		$response = $this->ManageAdmission_model->insertDetails();
		if($response ==1){
			$userdata['status'] = array('status' => "1", "message" => "Admission Details added successfully.");
			$userdata['data'] = $response;
        }else if($response ==-1){
			$userdata['status'] = array('status' => "0", "message" => "Admission Details already saved.");
			
		}else{
			$userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}
    
    public function getDetails()
    {
		$userList = $this->ManageAdmission_model->getDetails();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
    }

    public function getCourseDeatils()
    {
		$userList = $this->ManageAdmission_model->getCourseDeatils();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
    }
    
	public function onChangeSearch()
    {
		$userList = $this->ManageAdmission_model->onChangeSearch();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
	}
	
	public function updateDetails()
	{
        $response = $this->ManageAdmission_model->updateDetails();
        if($response == 1){
                $Formsdata['status'] = array('status' => "1", "message" => "Admission details updated successfully.");
                $Formsdata['data'] = $response;
        }else if($response == -1){
                $Formsdata['status'] = array('status' => "0", "message" => "Nothing to update.");
        }else{
                $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
 
	public function deleteDetails()
	{
		$admissionId = $this->input->post('admissionId');
		$user = $this->ManageAdmission_model->deleteDetails($admissionId);
        $response = array(
            'message' => 'Admission Details deleted successfully.'
        );
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }
    
    public function getAccessPermissions()
    {
		$userList = $this->ManageAdmission_model->getAccessPermissions();
		$this->output->set_content_type('application/json')->set_output(json_encode($userList));
    }
    
    public function uploadExcel(){
        $finalarray = array();
        $retrunBit = 0;
        $returnMessage="";
        $file_data = $_FILES['admissionExcelFile'];       
        $file_path = $file_data['tmp_name'];

        $this->load->library('EXcel');
        $inputFileName = $file_path;
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
		$arrayCount = count($allDataInSheet);
		

        for ($i = 2; $i <= $arrayCount; $i++) {
			
			if(empty($allDataInSheet[$i]["A"]) && empty($allDataInSheet[$i]["B"]) && empty($allDataInSheet[$i]["C"]) && empty($allDataInSheet[$i]["D"]) && empty($allDataInSheet[$i]["E"]) && empty($allDataInSheet[$i]["F"]) && empty($allDataInSheet[$i]["G"]) && empty($allDataInSheet[$i]["H"]) && empty($allDataInSheet[$i]["I"]) && empty($allDataInSheet[$i]["J"]) && empty($allDataInSheet[$i]["K"]) && empty($allDataInSheet[$i]["L"]) && empty($allDataInSheet[$i]["M"]) && empty($allDataInSheet[$i]["N"]) && empty($allDataInSheet[$i]["O"]) && empty($allDataInSheet[$i]["P"]) && empty($allDataInSheet[$i]["Q"]) && empty($allDataInSheet[$i]["R"]) && empty($allDataInSheet[$i]["S"]) && empty($allDataInSheet[$i]["T"]) && empty($allDataInSheet[$i]["U"]) && empty($allDataInSheet[$i]["V"]) && empty($allDataInSheet[$i]["W"]) && empty($allDataInSheet[$i]["X"]) && empty($allDataInSheet[$i]["Y"]) && empty($allDataInSheet[$i]["Z"]) && empty($allDataInSheet[$i]["AA"]) && empty($allDataInSheet[$i]["AB"])&& empty($allDataInSheet[$i]["AC"]) && empty($allDataInSheet[$i]["AD"])&& empty($allDataInSheet[$i]["AE"]) && empty($allDataInSheet[$i]["AF"]) && empty($allDataInSheet[$i]["AG"]) && empty($allDataInSheet[$i]["AH"]) && empty($allDataInSheet[$i]["AI"]) && empty($allDataInSheet[$i]["AJ"]) && empty($allDataInSheet[$i]["AK"]) && empty($allDataInSheet[$i]["AL"]) && empty($allDataInSheet[$i]["AM"]) && empty($allDataInSheet[$i]["AN"])){
				$retrunBit = 1;  
			    break;
			}else{
					if (!empty($allDataInSheet[$i]["A"])) {
						if (!empty($allDataInSheet[$i]["B"])) {
							if (!empty($allDataInSheet[$i]["C"])) {
								// if (!empty($allDataInSheet[$i]["D"])) {
									if (!empty($allDataInSheet[$i]["E"])) {
										if (!empty($allDataInSheet[$i]["F"])) {
											$individualArray = array(
												'student_name'=> trim($allDataInSheet[$i]["A"]), 
												'email_id'=> trim($allDataInSheet[$i]["B"]), 
												'mobile_number' => trim($allDataInSheet[$i]["C"]), 
												'alter_mobile_number' => trim($allDataInSheet[$i]["D"]),
												'selected_course' => trim($allDataInSheet[$i]["E"]), 
												'admission_status' => trim($allDataInSheet[$i]["F"])
												
											);
											$finalarray[] = $individualArray;
											$retrunBit = 1;
										}else{
										$retrunBit = 2;
										$returnMessage = "Your provided excel sheet contains empty value on column 'Admission Status' & row number ".$i;
										break;
										}
									}else{
									$retrunBit = 2;
									$returnMessage = "Your provided excel sheet contains empty value on column 'Course Name'  & row number ".$i;
									break;
									}
								// }else{
								// $retrunBit = 2;
								// $returnMessage = "Your provided excel sheet contains empty value on column 'Alternative mobile number' & row number ".$i;
								// break;
								// }
							}else{
							$retrunBit = 2;
							$returnMessage = "Your provided excel sheet contains empty value on column 'Mobile Number' & row number ".$i;
							break;
							}
						
						}else{
						$retrunBit = 2;
						$returnMessage = "Your provided excel sheet contains empty value on column 'Email Id' & row number ".$i;
						break;
						}
					}else{
					$retrunBit = 2;
					$returnMessage = "Your provided excel sheet contains empty value on column 'Student Name'  & row number ".$i;
					break;
					}
				
            }
        }
		if($retrunBit == 1){
				$response = $this->ManageAdmission_model->excelLeadsUpload($finalarray);
				if($response == 1){
					$userdata['status'] = array('status' => "1", "message" => "Admission details added successfully.");
					$userdata['data'] = $response;
				}else if($response == -1){
						$userdata['status'] = array('status' => "0", "message" => "Admission details already exists.");
				}else{
						$userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
				}
			//$userdata['status'] = array('status' => "1", "message" => "Student excel uploaded successfully.");
		}else if($retrunBit == 2){
			$userdata['status'] = array('status' => "0", "message" => $returnMessage);
		}else if($retrunBit == -1){
                    $userdata['status'] = array('status' => "0",  "message" => "Admission details already exists.");
                }else{
                    $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
	   
	   $this->output->set_content_type('application/json')->set_output(json_encode($userdata));
    }

    
}

