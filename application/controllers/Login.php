<?php

// defined('BASEPATH') OR exit('No direct script access allowed');
// header('Access-Control-Allow-Origin: *');
// header("Access-Control-Allow-Credentials", "true");
// header("Access-Control-Max-Age", "1800");
// header("Access-Control-Allow-Headers", "content-type,X-Requested-With");
// header("Access-Control-Allow-Methods","PUT, POST, GET, DELETE, PATCH, OPTIONS");

Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
class Login extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Login_model');
    }

    

   	public function checkLogin()
    {
        $response = $this->Login_model->checkLogin();
        if(is_array($response)){
            $userdata['status'] = array('status' => "1", "message" => "login successfull.");
            $userdata['data'] = $response;
        }else if($response==2){
            $userdata['status'] = array('status' => "0", "message" => "Waiting for admin approval.");
        }else if($response == -2){
            $userdata['status'] = array('status' => "0", "message" => "Request sent to admin approval.");
        }else if($response == -4){
            $userdata['status'] = array('status' => "0", "message" => "Admin has denied your request.");
        }else if($response == -3){
            $userdata['status'] = array('status' => "0", "message" => "Invalid login details");
        }else if($response == -5){
            $userdata['status'] = array('status' => "0", "message" => "Previously you are logged in from other machine.your new machine access request sent to admin approval");
        }else{
            $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }

		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));

	}



	public function ChangePassword()
    {
        $response = $this->Login_model->ChangePassword();
        if($response == 1){
            $passworddata['status'] = array('status' => "1", "message" => "Password Updated successfully.");
            $passworddata['data'] = $response;
        }else{
            $passworddata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($passworddata));

    }

     public function forgotPassword()
	{
		$response = $this->Login_model->forgotPassword();
		if($response == 1){
			$userdata['status'] = array('status' => "1", "message" => "Please check your email for password.");
			$userdata['data'] = $response;
		}else if($response == 2){
			$userdata['status'] = array('status' => "0", "message" => "Provided email not register with us");
		}else{
			$userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}

    
}

