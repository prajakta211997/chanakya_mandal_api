<?php 

class ManageRole_model extends CI_Model {

    
    function insertDetails(){
        $query = $this->db->get_where('manage_role', array(
               'role_name'=>  $_POST['roleName'], 
                'delete_bit'=>'0'));
        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data = array(
                'role_name'=>  $_POST['roleName'], 
            );
            $insert = $this->db->insert('manage_role', $data);
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    }

    function getDetails(){
        
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_role');
        return $query->result();
    }

    function onChangeSearch(){
        $searchData = $_POST['searchData'];
        $this->db->select("*");
        $this->db->from("manage_role");
        $this->db->like('role_name', $searchData);
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result();
    }

    function updateDetails(){
        
        $query = $this->db->get_where('manage_role', array(
            'role_name'=>  $_POST['roleName'], 
            'delete_bit'=>'0'));

        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data =  array(
                'role_name'=>  $_POST['roleName'], 
            );
            $this->db->where('role_id', $_POST['roleId']);
            $this->db->update('manage_role', $data);
            return true;
        }
    
    }
    
    function deleteRole($roleId){
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('role_id', $roleId);
        $this->db->update('manage_role', $data);
        // $this->db->where('role_id', $roleId);
        // $this->db->delete('manage_role');
       
    }
    
    function getAccessPermissions(){
        $this->db->select("*");
        $this->db->where('user_id',$_POST['userId']);
        $this->db->where('aceess_screen',$_POST['ManageUser']);
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_access');
        return $query->result();
    }

    

    
}

?>