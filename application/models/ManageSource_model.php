<?php 

class ManageSource_model extends CI_Model {

    
    function insertDetails(){
        $query = $this->db->get_where('manage_source', array(
               'source_name'=>  $_POST['sourceName'], 
                'delete_bit'=>'0'));
        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data = array(
                'source_name'=>  $_POST['sourceName'], 
            );
            $insert = $this->db->insert('manage_source', $data);
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    }

    function getDetails(){
        
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_source');
        return $query->result();
    }

    function onChangeSearch(){
        $searchData = $_POST['searchData'];
        $this->db->select("*");
        $this->db->from("manage_source");
        $this->db->like('source_name', $searchData);
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result();
    }

    function updateDetails(){
        
        $query = $this->db->get_where('manage_source', array(
            'source_name'=>  $_POST['sourceName'], 
            'delete_bit'=>'0'));

        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data =  array(
                'source_name'=>  $_POST['sourceName'], 
            );
            $this->db->where('source_id', $_POST['sourceId']);
            $this->db->update('manage_source', $data);
            return true;
        }
    
    }
    
    function deleteSource($sourceId){
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('source_id', $sourceId);
        $this->db->update('manage_source', $data);
    }
    
    function getAccessPermissions(){
        $this->db->select("*");
        $this->db->where('user_id',$_POST['userId']);
        $this->db->where('aceess_screen',$_POST['ManageUser']);
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_access');
        return $query->result();
    }
    

    

    
}

?>