<?php 

class ManageAccess_model extends CI_Model {
    
    function insertDetails(){
        $query = $this->db->get_where('manage_access', array(
               'user_id'=>  $_POST['selectUser'], 
               'aceess_screen'=>  $_POST['selectScreen'], 
               'delete_permission' =>$_POST['DeleteAccess'],
               'edit_permission'=> $_POST['EditAccess'],
               'view_permission' => $_POST['ViewAccess'],
               'delete_bit'=>'0'));
        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data = array(
                'user_id'=>  $_POST['selectUser'], 
               'aceess_screen'=>  $_POST['selectScreen'],
               'delete_permission' =>$_POST['DeleteAccess'],
               'edit_permission'=> $_POST['EditAccess'],
               'view_permission' => $_POST['ViewAccess'],
            );
            $insert = $this->db->insert('manage_access', $data);
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    }

    function getDetails(){
        
        $this->db->select("*");
        $this->db->from("manage_access ma");
        $this->db->where('ma.delete_bit', '0');
        $this->db->where('mc.delete_bit', '0');
        $this->db->join('manage_user mc',"mc.user_id = ma.user_id");
        return $this->db->get()->result();
    }

    function getUserDeatils(){
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_user');
        return $query->result();
    }

    function onChangeSearch(){
        $searchData = $_POST['searchData'];
        $this->db->select("*");
        $this->db->from("manage_access ma");
        $this->db->like('mu.user_name', $searchData);
        $this->db->where('mu.delete_bit', '0');
        $this->db->where('ma.delete_bit', '0');
        $this->db->join('manage_user mu',"mu.user_id = ma.user_id");
        return $this->db->get()->result();
    }

    function updateDetails(){
        
        $query = $this->db->get_where('manage_access', array(
            'user_id'=>  $_POST['selectUser'], 
            'aceess_screen'=>  $_POST['selectScreen'],       
            'delete_permission' =>$_POST['DeleteAccess'],
            'edit_permission'=> $_POST['EditAccess'],
            'view_permission' => $_POST['ViewAccess'], 
            'delete_bit'=>'0'));

        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data =  array(
                'user_id'=>  $_POST['selectUser'], 
               'aceess_screen'=>  $_POST['selectScreen'],
               'delete_permission' =>$_POST['DeleteAccess'],
               'edit_permission'=> $_POST['EditAccess'],
               'view_permission' => $_POST['ViewAccess'],
            );
            $this->db->where('access_id', $_POST['accessId']);
            $this->db->update('manage_access', $data);
            return true;
        }
    
    }
    
    function deleteDetails($accessId){
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('access_id', $accessId);
        $this->db->update('manage_access',$data);
       
    }
    
    function getAccessPermissions(){
        $this->db->select("*");
        $this->db->where('user_id',$_POST['userId']);
        $this->db->where('aceess_screen',$_POST['ManageUser']);
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_access');
        return $query->result();
    }

    

    
}

?>