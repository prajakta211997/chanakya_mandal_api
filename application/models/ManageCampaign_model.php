<?php 

class ManageCampaign_model extends CI_Model {
    
    function insertDetails(){
        $query = $this->db->get_where('manage_campaign', array(
               'campaign_name'=>  $_POST['campaignName'], 
               'start_date'=>  $_POST['startDate'], 
               'end_date'=>  $_POST['endDate'], 
               'delete_bit'=>'0'));
        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data = array(
                'campaign_name'=>  $_POST['campaignName'], 
               'start_date'=>  $_POST['startDate'], 
               'end_date'=>  $_POST['endDate'], 
               
            );
            $insert = $this->db->insert('manage_campaign', $data);
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    }

    function getDetails(){
        
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_campaign');
        return $query->result();
    }

    function onChangeSearch(){
        $searchData = $_POST['searchData'];
        $this->db->select("*");
        $this->db->from("manage_campaign");
        $this->db->like('campaign_name', $searchData);
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result();
    }

    function updateDetails(){
        
        $query = $this->db->get_where('manage_campaign', array(
            'campaign_name'=>  $_POST['campaignName'], 
            'start_date'=>  $_POST['startDate'], 
            'end_date'=>  $_POST['endDate'], 
            'delete_bit'=>'0'));

        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data =  array(
                'campaign_name'=>  $_POST['campaignName'], 
               'start_date'=>  $_POST['startDate'], 
               'end_date'=>  $_POST['endDate'], 
               
            );
            $this->db->where('campaign_id', $_POST['campaignId']);
            $this->db->update('manage_campaign', $data);
            return true;
        }
    
    }
    
    function deleteDetails($campaignId){
        
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('campaign_id', $campaignId);
        $this->db->update('manage_campaign', $data);
       
        // $this->db->where('campaign_id', $campaignId);
        // $this->db->delete('manage_campaign');
       
    }
    
    function getAccessPermissions(){
        $this->db->select("*");
        $this->db->where('user_id',$_POST['userId']);
        $this->db->where('aceess_screen',$_POST['ManageUser']);
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_access');
        return $query->result();
    }

    

    
}

?>