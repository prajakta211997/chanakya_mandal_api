<?php 

class ManageUser_model extends CI_Model {

    function insertDetails(){
      
        $location = "uploads/".$_POST['userName'];
        if(!is_dir($location)){
            mkdir($location,0755,TRUE);
        }

        if(isset($_FILES['userPhoto']) && !empty( $_FILES["userPhoto"])){
            $photo_data = $_FILES['userPhoto'];
            $photoname = $_FILES['userPhoto']['name']; 
            $photolocation = $location."/".$photoname;
            move_uploaded_file($_FILES['userPhoto']['tmp_name'],$photolocation);
            
        } 
        if(isset($_FILES['userPhoto'])){
            $photoloc = $photolocation;
        }else{
            $photoloc = '';
        }
        
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $password = substr( str_shuffle( $chars ), 0, 6 );

        $query = $this->db->get_where('manage_user', array(
            'user_email_id'=> $_POST['emailId'], 
            'user_mobile_number' => $_POST['mobileNumber'], 
            'user_role' => $_POST['userRole'],
            'delete_bit'=>'0'));

        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data = array(
                'user_name'=>  $_POST['userName'], 
                'user_email_id'=> $_POST['emailId'], 
                'user_mobile_number' => $_POST['mobileNumber'], 
                'user_role' => $_POST['userRole'],
                'user_birth_date'=> $_POST['userBirthDate'],
                'user_address'=> $_POST['userAddress'],
                'user_uploaded_photo'=> $photoloc,
                'user_password'=> $password,
            );
            
            $insert = $this->db->insert('manage_user', $data);
                return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    }

    
    function getDetails(){
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_user');
        return $query->result();
    }

    function getRoleDetails(){
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_role');
        return $query->result();
    }

    function getAccessPermissions(){
        $this->db->select("*");
        $this->db->where('user_id',$_POST['userId']);
        $this->db->where('aceess_screen',$_POST['ManageUser']);
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_access');
        return $query->result();
    }
    
    function onChangeSearch(){
        $searchData = $_POST['searchData'];
        $this->db->select("*");
        $this->db->from("manage_user");
        $this->db->like('user_name', $searchData);
        $this->db->or_like('user_email_id', $searchData);
        $this->db->or_like('user_mobile_number', $searchData);
        $this->db->or_like('user_role', $searchData);
        $this->db->or_like('user_address', $searchData);
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result();
        
    }

    function updateDetails(){
        $location = "uploads/".$_POST['userName'];
        if(!is_dir($location)){
            mkdir($location,0755,TRUE);
        }

        if(isset($_FILES['userPhoto']) && !empty( $_FILES["userPhoto"])){
            $photo_data = $_FILES['userPhoto'];
            $photoname = $_FILES['userPhoto']['name']; 
            $photolocation = $location."/".$photoname;
            move_uploaded_file($_FILES['userPhoto']['tmp_name'],$photolocation);
            
        } 
        
        $query = $this->db->get_where('manage_user', array(
            'user_name'=>  $_POST['userName'], 
            'user_email_id'=> $_POST['emailId'], 
            'user_mobile_number' => $_POST['mobileNumber'], 
            'user_role' => $_POST['userRole'],
            'user_birth_date'=> $_POST['userBirthDate'],
            'user_address'=> $_POST['userAddress'],
            'user_password'=> $_POST['userPassword'],
            'delete_bit'=>'0'));

            if(isset($_FILES['userPhoto'])){
                $query = $this->db->get_where('manage_user', array('user_uploaded_photo'=>$photolocation));
            }

            if ($query->num_rows() == 1) {
            return -1;
            }else{
                $data =  array(
                    'user_name'=>  $_POST['userName'], 
                    'user_email_id'=> $_POST['emailId'], 
                    'user_mobile_number' => $_POST['mobileNumber'], 
                    'user_role' => $_POST['userRole'],
                    'user_birth_date'=> $_POST['userBirthDate'],
                    'user_address'=> $_POST['userAddress'],
                    'user_password'=> $_POST['userPassword'],
                );
                $this->db->where('user_id', $_POST['userId']);
                $this->db->update('manage_user', $data);

                if(isset($_FILES['userPhoto'])){
                    $photo =array('user_uploaded_photo'=>$photolocation);
                    $photoArray = array_merge($data,$photo);
                    $this->db->where('user_id', $_POST['userId']);
                    $this->db->update('manage_user', $photoArray);
                }
                return true;
             }
    
    }
    
    function deleteUser($userId){
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('user_id', $userId);
        $this->db->update('manage_user', $data);

        
       
    }

    
    

    

    
}

?>