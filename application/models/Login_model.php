<?php 
class Login_model extends CI_Model {
    function checkLogin(){
        $input_data = json_decode(trim(file_get_contents('php://input')), true);
        // $userName = $input_data['email_id'];
        // $userPassword = $this->my_simple_crypt($input_data['password'],'e');


        //  $query = $this->db->get_where('user_login', array('user_email' => $userName, 'user_password' => $userPassword ,'user_delete_bit' => '0'));

        // if ($query->num_rows() == 1) {
        //     $loginArrayFromDB = $query->result_array();
            
        //     foreach($loginArrayFromDB as $loginArrayv){
        //         if($loginArrayv['user_role'] == 'admin'){
                    
        //             if($loginArrayv['user_admin_approval_bit']==0){
        //                 return 2;
        //             }else if($loginArrayv['user_admin_approval_bit'] == '2') {
        //                 return -4;
        //             }else{

        //                 $newdata = array(
        //                     'user_id' => $loginArrayv['user_id'],
        //                     'user_name' => $loginArrayv['user_name'],
        //                     'user_role' => $loginArrayv['user_role'],
        //                     'logged_in' => TRUE,
        //                 );        

        //                 $this->session->set_userdata($newdata);
        //                 return $newdata;

        //                 }
        //         }
        //         else{

        //             $newdata = array(
        //                 'user_id' => $loginArrayv['user_id'],
        //                 'user_name' => $loginArrayv['user_name'],
        //                 'user_role' => $loginArrayv['user_role'],
        //                 'logged_in' => TRUE,
        //                 //  
        //             );        

        //             $this->session->set_userdata($newdata);
        //             return $newdata;

        //         }

        //     }

        // } 

        // else {
        //     return -3;

        // }
        $userName = $input_data['email_id'];
        $userPassword = $input_data['password'];

        $query = $this->db->get_where('manage_user', array('user_email_id' => $userName, 'user_password' => $userPassword ,'delete_bit' => '0'));

        if ($query->num_rows() == 1) {
            $loginArrayFromDB = $query->result_array();
            
            foreach($loginArrayFromDB as $loginArrayv){
                
                
                $this->db->select("*");
                $this->db->from('manage_access');
                $this->db->where('user_id', $loginArrayv['user_id']);
                $this->db->where('delete_bit', '0');
                $acessData = $this->db->get()->result_array();
                $newdata = array(
                    'user_id' => $loginArrayv['user_id'],
                    'user_name' => $loginArrayv['user_name'],
                    'user_role' => $loginArrayv['user_role'],
                    'logged_in' => TRUE,
                    'accessScreen' => $acessData,
                );        
                $this->session->set_userdata($newdata);
                return $newdata;
            }
        }
    }



    
    function ChangePassword(){ 

        $oldPass = $_POST['oldpassword'];
        $newPass = $_POST['newpassword'];
        $data = array(
        'user_password' => $newPass,

        );

        $this->db->where('user_id', $_POST['userId']);
        $this->db->update('manage_user', $data);
        return true;

        }

    function my_simple_crypt( $string, $action) {
        $secret_key = 'whitecode_key';
        $secret_iv = 'whitecode_iv';

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }

        return $output;

    }

    function forgotPassword(){
        $email_id = $_POST['email_id'];
        $query = $this->db->get_where('manage_user', array('user_email_id' => $email_id));
        if ($query->num_rows() == 1) {
            $loginArrayFromDB = $query->result_array();
            foreach($loginArrayFromDB as $loginArrayv){
                
                // $result['name']= $loginArrayv['Teacher_first_name']." ".$loginArrayv['Teacher_last_name'];
                // $result['userPassword']= $loginArrayv['original_password'];
                // $result['email']= $email_id;
                // $result['subject'] = 'Forgot Password Request';
                // $template = 'email-template/user-forgot-password';
                // $sendMail = sendUserDetailsMail($result,$template);

                // $result1['name']= $loginArrayv['user_name'];
                // $result1['userPassword']= $this->my_simple_crypt($loginArrayv['user_password'],'d');
                // $result1['email']= admin_email;
                // $result1['subject'] = 'Forgot Password Request By User';
                // $template1 = 'email-template/admin-forgot-password';
                // $sendMail1 = sendUserDetailsMail($result1,$template1);
                return 1;
            }
        } 
        else {
            return 2;
        }
    }

}



?>