<?php 

class ManageLeaves_model extends CI_Model {
    
  
    function insertDetails(){
        $query = $this->db->get_where('manage_leave', array(
               'selected_user'=>  $_POST['selectUser'], 
               'leave_type'=>  $_POST['leaveType'],
               'leave_reason'=>  $_POST['leaveReason'],
               'start_date'=>  $_POST['startDate'],
               'end_date'=>  $_POST['endDate'],
               'no_of_days'=>  $_POST['leaveDays'], 
               'delete_bit'=>'0'));
        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data = array(
                'selected_user'=>  $_POST['selectUser'], 
               'leave_type'=>  $_POST['leaveType'],
               'leave_reason'=>  $_POST['leaveReason'],
               'start_date'=>  $_POST['startDate'],
               'end_date'=>  $_POST['endDate'],
               'no_of_days'=>  $_POST['leaveDays'], 
            );
            $insert = $this->db->insert('manage_leave', $data);
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    }

    function getDetails(){
        
        $this->db->select("*");
        $this->db->from("manage_leave ma");
        $this->db->where('ma.delete_bit', '0');
        $this->db->where('mc.delete_bit', '0');
        $this->db->join('manage_user mc',"mc.user_id = ma.selected_user");
        return $this->db->get()->result();
    }

    function getUserDeatils(){
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_user');
        return $query->result();
    }

    function onChangeSearch(){
        $searchData = $_POST['searchData'];
        $this->db->select("*");
        $this->db->from("manage_leave ma");
        $this->db->like('mu.user_name', $searchData);
        $this->db->where('mu.delete_bit', '0');
        $this->db->where('ma.delete_bit', '0');
        $this->db->join('manage_user mu',"mu.user_id = ma.selected_user");
        return $this->db->get()->result();
    }

    function updateDetails(){
        
        $query = $this->db->get_where('manage_leave', array(
            'selected_user'=>  $_POST['selectUser'], 
            'leave_type'=>  $_POST['leaveType'],
            'leave_reason'=>  $_POST['leaveReason'],
            'start_date'=>  $_POST['startDate'],
            'end_date'=>  $_POST['endDate'],
            'no_of_days'=>  $_POST['leaveDays'], 
            'delete_bit'=>'0'));

        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data =  array(
                'selected_user'=>  $_POST['selectUser'], 
                'leave_type'=>  $_POST['leaveType'],
                'leave_reason'=>  $_POST['leaveReason'],
                'start_date'=>  $_POST['startDate'],
                'end_date'=>  $_POST['endDate'],
                'no_of_days'=>  $_POST['leaveDays'], 
            );
            $this->db->where('leave_id', $_POST['leaveId']);
            $this->db->update('manage_leave', $data);
            return true;
        }
    
    }
    
    function deleteDetails($leaveId){
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('leave_id', $leaveId);
        $this->db->update('manage_leave',$data);
       
    }
    
    function getAccessPermissions(){
        $this->db->select("*");
        $this->db->where('user_id',$_POST['userId']);
        $this->db->where('aceess_screen',$_POST['ManageUser']);
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_access');
        return $query->result();
    }

    

    
}

?>