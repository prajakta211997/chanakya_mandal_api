<?php 

class ManageCourse_model extends CI_Model {
    
    function insertDetails(){
        $query = $this->db->get_where('manage_course', array(
               'course_name'=>  $_POST['courseName'], 
               'course_type'=>  $_POST['courseType'], 
               'course_duration'=>  $_POST['courseDuration'], 
               'start_date'=>  $_POST['startDate'], 
               'end_date'=>  $_POST['endDate'], 
               'subject_name'=>  $_POST['subjectName'], 
               'course_fees'=>  $_POST['courseFees'],
               'course_description'=>  $_POST['courseDescription'],
                'delete_bit'=>'0'));
        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data = array(
                'course_name'=>  $_POST['courseName'], 
               'course_type'=>  $_POST['courseType'], 
               'course_duration'=>  $_POST['courseDuration'], 
               'start_date'=>  $_POST['startDate'], 
               'end_date'=>  $_POST['endDate'], 
               'subject_name'=>  $_POST['subjectName'], 
               'course_fees'=>  $_POST['courseFees'],
               'course_description'=>  $_POST['courseDescription'],
            );
            $insert = $this->db->insert('manage_course', $data);
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    }

    function getDetails(){
        
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_course');
        return $query->result();
    }

    function onChangeSearch(){
        $searchData = $_POST['searchData'];
        $this->db->select("*");
        $this->db->from("manage_course");
        $this->db->like('course_name', $searchData);
        $this->db->or_like('course_type', $searchData);
        $this->db->or_like('subject_name', $searchData);
        $this->db->or_like('course_fees', $searchData);
        $this->db->or_like('course_description', $searchData);
      
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result();
    }

    function updateDetails(){
        
        $query = $this->db->get_where('manage_course', array(
            'course_name'=>  $_POST['courseName'], 
            'course_type'=>  $_POST['courseType'], 
            'course_duration'=>  $_POST['courseDuration'], 
            'start_date'=>  $_POST['startDate'], 
            'end_date'=>  $_POST['endDate'], 
            'subject_name'=>  $_POST['subjectName'], 
            'course_fees'=>  $_POST['courseFees'],
            'course_description'=>  $_POST['courseDescription'], 
            'delete_bit'=>'0'));

        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data =  array(
                'course_name'=>  $_POST['courseName'], 
               'course_type'=>  $_POST['courseType'], 
               'course_duration'=>  $_POST['courseDuration'], 
               'start_date'=>  $_POST['startDate'], 
               'end_date'=>  $_POST['endDate'], 
               'subject_name'=>  $_POST['subjectName'], 
               'course_fees'=>  $_POST['courseFees'],
               'course_description'=>  $_POST['courseDescription'],
            );
            $this->db->where('course_id', $_POST['courseId']);
            $this->db->update('manage_course', $data);
            return true;
        }
    
    }
    
    function deleteDetails($courseId){
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('course_id', $courseId);
        $this->db->update('manage_course',$data);
       
    }
    
    function getAccessPermissions(){
        $this->db->select("*");
        $this->db->where('user_id',$_POST['userId']);
        $this->db->where('aceess_screen',$_POST['ManageUser']);
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_access');
        return $query->result();
    }
    

    

    
}

?>