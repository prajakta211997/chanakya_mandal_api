<?php 

class ManageLead_model extends CI_Model {
    
    function insertDetails(){
        $query = $this->db->get_where('manage_lead', array(
               'student_name'=>  $_POST['studentName'], 
               'mobile_number'=>  $_POST['mobileNumber'], 
               'alter_mobile_number'=>  $_POST['alterMobileNumber'], 
               'selected_course'=>  $_POST['selectcourse'], 
               'selected_source'=>  $_POST['selectsource'], 
               'campaign_name'=>  $_POST['campaignName'], 
               'date'=>  $_POST['receivedDate'], 
               'email_id'=>  $_POST['emailId'], 
               'delete_bit'=>'0'));
        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data = array(
                'student_name'=>  $_POST['studentName'], 
               'mobile_number'=>  $_POST['mobileNumber'], 
               'alter_mobile_number'=>  $_POST['alterMobileNumber'], 
               'selected_course'=>  $_POST['selectcourse'], 
               'selected_source'=>  $_POST['selectsource'], 
               'campaign_name'=>  $_POST['campaignName'], 
               'date'=>  $_POST['receivedDate'], 
               'email_id'=>  $_POST['emailId'], 
            );
            $insert = $this->db->insert('manage_lead', $data);
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    }

    function getDetails(){
        
        // $this->db->select("*");
        // $this->db->from("manage_lead ma");
        // $this->db->where('ma.delete_bit', '0');
        // $this->db->where('mc.delete_bit', '0');
        // $this->db->join('manage_course mc',"mc.course_id = ma.selected_course");
        // return $this->db->get()->result();
        $this->db->select("*");
        $this->db->from("manage_lead ml");
        $this->db->where('ml.delete_bit', '0');
       // $this->db->join('manage_user mu',"mu.user_id = ml.assign_lead");
        return $this->db->get()->result();
    }

    

    function onChangeSearch(){
        $searchData = $_POST['searchData'];
        $this->db->select("*");
        $this->db->from("manage_lead ma");
        $this->db->like('ma.student_name', $searchData);
        $this->db->or_like('ma.email_id', $searchData);
        //$this->db->or_like('mc.course_name', $searchData);
        //$this->db->where('mc.delete_bit', '0');
        $this->db->where('ma.delete_bit', '0');
        $this->db->join('manage_user mu',"mu.user_id = ma.assign_lead");
        return $this->db->get()->result();
    }

    function updateDetails(){
        
        $query = $this->db->get_where('manage_lead', array(
            'student_name'=>  $_POST['studentName'], 
            'mobile_number'=>  $_POST['mobileNumber'], 
            'alter_mobile_number'=>  $_POST['alterMobileNumber'], 
            'selected_course'=>  $_POST['selectcourse'], 
            'selected_source'=>  $_POST['selectsource'], 
            'campaign_name'=>  $_POST['campaignName'], 
            'date'=>  $_POST['receivedDate'], 
            'email_id'=>  $_POST['emailId'],  
            'delete_bit'=>'0'));

        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data =  array(
                'student_name'=>  $_POST['studentName'], 
                'mobile_number'=>  $_POST['mobileNumber'], 
                'alter_mobile_number'=>  $_POST['alterMobileNumber'], 
                'selected_course'=>  $_POST['selectcourse'], 
                'selected_source'=>  $_POST['selectsource'], 
                'campaign_name'=>  $_POST['campaignName'], 
                'date'=>  $_POST['receivedDate'], 
                'email_id'=>  $_POST['emailId'], 
            );
            $this->db->where('lead_id', $_POST['leadId']);
            $this->db->update('manage_lead', $data);
            return true;
        }
    
    }
    
    function deleteDetails($leadId){
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('lead_id', $leadId);
        $this->db->update('manage_lead',$data);
       
    }
    
    function getUserDeatils(){
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $this->db->where('user_role', 'Telecaller');
        $query = $this->db->get('manage_user');
        return $query->result();
    }
    function getCourseDeatils(){
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_course');
        return $query->result();
    }
    function getSourceDeatils(){
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_source');
        return $query->result();
    }
    
    
    function excelLeadsUpload($finalarray, $selectUser){
      //  print_r($finalarray);
       $this->db->select("*");
       $this->db->from('manage_user');
       $this->db->where('user_role', 'Telecaller');
       $this->db->where('delete_bit', '0');
       $userData = $this->db->get()->result_array();
       
       $finalArrayCount = count($finalarray);
       $userDataCount = count($userData);
       $splitcount = floor($finalArrayCount / $userDataCount);
       //print_r($userData);
      if($selectUser == 'Distribute Equally'){
            $assign_lead_array = array_chunk($finalarray, $userDataCount);
            for($i=0;$i<count($assign_lead_array);$i++){
                for($j=0;$j<$userDataCount;$j++){
                    if(! empty($assign_lead_array[$i][$j])){
                        $data = array(
                            'assign_lead' => $userData[$j]['user_id'],
                            'student_name'=>  $assign_lead_array[$i][$j]['student_name'], 
                            'mobile_number'=>  $assign_lead_array[$i][$j]['mobile_number'], 
                            'alter_mobile_number'=>  $assign_lead_array[$i][$j]['alter_mobile_number'], 
                            'selected_course'=> $assign_lead_array[$i][$j]['selected_course'], 
                            'selected_source'=>  $assign_lead_array[$i][$j]['selected_source'], 
                            'campaign_name'=>  $assign_lead_array[$i][$j]['campaign_name'], 
                            'date'=>  $assign_lead_array[$i][$j]['date'], 
                            'email_id'=>  $assign_lead_array[$i][$j]['email_id'], 
                        );
                    }else{
                        break;
                    }
                    $finalDataArray[] =$data;
                }
               
             }
             
             $this->db->insert_batch('manage_lead',$finalDataArray);
             return true;

      } else if($selectUser == 'Distribute Randomly'){
            $this->db->select("*");
            $this->db->from('manage_user');
            $this->db->where('user_role', 'Telecaller');
            $this->db->where('delete_bit', '0');
            $userData = $this->db->get()->result_array();

            $keys = array_keys($finalarray); 
            shuffle($finalarray); 
            $shuffledArray = array();
            foreach($keys as $key) {
            $shuffledArray[$key] = $finalarray[$key]; //Get the original array using keys from shuffled array
            }

            $userkeys = array_keys($userData); 
            shuffle($userData); 
            $shuffledUserArray = array();
            foreach($userkeys as $key1) {
            $shuffledUserArray[$key1] = $userData[$key1]; //Get the original array using keys from shuffled array
            }
            $assign_lead_array = array_chunk($shuffledArray, count($shuffledUserArray));
            for($i=0;$i<count($assign_lead_array);$i++){
                for($j=0;$j<count($userData);$j++){
                    if(! empty($assign_lead_array[$i][$j])){
                        $data = array(
                            'assign_lead' => $shuffledUserArray[$j]['user_id'],
                            'student_name'=>  $assign_lead_array[$i][$j]['student_name'], 
                            'mobile_number'=>  $assign_lead_array[$i][$j]['mobile_number'], 
                            'alter_mobile_number'=>  $assign_lead_array[$i][$j]['alter_mobile_number'], 
                            'selected_course'=> $assign_lead_array[$i][$j]['selected_course'], 
                            'selected_source'=>  $assign_lead_array[$i][$j]['selected_source'], 
                            'campaign_name'=>  $assign_lead_array[$i][$j]['campaign_name'], 
                            'date'=>  $assign_lead_array[$i][$j]['date'], 
                            'email_id'=>  $assign_lead_array[$i][$j]['email_id'], 
                        );
                    }else{
                        break;
                    }
                    $finalDataArray[] =$data;
                }
               
             }
             $this->db->insert_batch('manage_lead',$finalDataArray);
             return true;
        } else{
            for($i=0;$i< count($finalarray);$i++){
                
                $data = array(
                    'assign_lead' => $selectUser,
                    'student_name'=>  $finalarray[$i]['student_name'], 
                    'mobile_number'=>  $finalarray[$i]['mobile_number'], 
                    'alter_mobile_number'=>  $finalarray[$i]['alter_mobile_number'], 
                    'selected_course'=> $finalarray[$i]['selected_course'], 
                    'selected_source'=>  $finalarray[$i]['selected_source'], 
                    'campaign_name'=>  $finalarray[$i]['campaign_name'], 
                    'date'=>  $finalarray[$i]['date'], 
                    'email_id'=>  $finalarray[$i]['email_id'], 
                );
                
                $finalDataArray[] =$data;
            }
            $this->db->insert_batch('manage_lead',$finalDataArray);
            return true;
        }
    }

    

      function getAccessPermissions(){
        $this->db->select("*");
        $this->db->where('user_id',$_POST['userId']);
        $this->db->where('aceess_screen',$_POST['ManageUser']);
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_access');
        return $query->result();
    }

    function indivisualAssignLeadsFunction(){
        
                
            $data = array(
                'assign_lead' => $_POST['selectUser']
            );
            
            $this->db->where('lead_id', $_POST['leadId']);
            $this->db->update('manage_lead',$data);
        return true;
    }
}

?>