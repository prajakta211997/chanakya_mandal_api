<?php 

class ManageAdmission_model extends CI_Model {
    
    function insertDetails(){
        $query = $this->db->get_where('manage_admission', array(
               'student_name'=>  $_POST['studentName'], 
               'mobile_number'=>  $_POST['mobileNumber'], 
               'alter_mobile_number'=>  $_POST['alterMobileNumber'], 
               'selected_course'=>  $_POST['selectcourse'], 
               'email_id'=>  $_POST['emailId'], 
               'admission_status'=>  $_POST['admissionStatus'], 
               'delete_bit'=>'0'));
        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data = array(
                'student_name'=>  $_POST['studentName'], 
                'mobile_number'=>  $_POST['mobileNumber'], 
                'alter_mobile_number'=>  $_POST['alterMobileNumber'], 
                'selected_course'=>  $_POST['selectcourse'], 
                'email_id'=>  $_POST['emailId'], 
                'admission_status'=>  $_POST['admissionStatus'], 
            );
            $insert = $this->db->insert('manage_admission', $data);
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    }

    function getDetails(){
        
        $this->db->select("*");
        $this->db->from("manage_admission ma");
        $this->db->where('ma.delete_bit', '0');
        $this->db->where('mc.delete_bit', '0');
        $this->db->join('manage_course mc',"mc.course_id = ma.selected_course");
        return $this->db->get()->result();
    }

    function getCourseDeatils(){
        $this->db->select("*");
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_course');
        return $query->result();
    }

    function onChangeSearch(){
        $searchData = $_POST['searchData'];
        $this->db->select("*");
        $this->db->from("manage_admission ma");
        $this->db->like('ma.student_name', $searchData);
        $this->db->or_like('ma.email_id', $searchData);
        $this->db->or_like('ma.admission_status', $searchData);
        $this->db->or_like('mc.course_name', $searchData);
        $this->db->where('mc.delete_bit', '0');
        $this->db->where('ma.delete_bit', '0');
        $this->db->join('manage_course mc',"mc.course_id = ma.selected_course");
        return $this->db->get()->result();
    }

    function updateDetails(){
        
        $query = $this->db->get_where('manage_admission', array(
            'student_name'=>  $_POST['studentName'], 
            'mobile_number'=>  $_POST['mobileNumber'], 
            'alter_mobile_number'=>  $_POST['alterMobileNumber'], 
            'selected_course'=>  $_POST['selectcourse'], 
            'email_id'=>  $_POST['emailId'], 
            'admission_status'=>  $_POST['admissionStatus'], 
            'delete_bit'=>'0'));

        if ($query->num_rows() == 1) {
            return -1;
        }else{
            $data =  array(
                'student_name'=>  $_POST['studentName'], 
                'mobile_number'=>  $_POST['mobileNumber'], 
                'alter_mobile_number'=>  $_POST['alterMobileNumber'], 
                'selected_course'=>  $_POST['selectcourse'], 
                'email_id'=>  $_POST['emailId'], 
                'admission_status'=>  $_POST['admissionStatus'], 
            );
            $this->db->where('admission_id', $_POST['admissionId']);
            $this->db->update('manage_admission', $data);
            return true;
        }
    
    }
    
    function deleteDetails($admissionId){
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('admission_id', $admissionId);
        $this->db->update('manage_admission',$data);
       
    }
    
    function getAccessPermissions(){
        $this->db->select("*");
        $this->db->where('user_id',$_POST['userId']);
        $this->db->where('aceess_screen',$_POST['ManageUser']);
        $this->db->where('delete_bit', '0');
        $query = $this->db->get('manage_access');
        return $query->result();
    }

    function excelLeadsUpload($finalarray){
        for($i=0;$i< count($finalarray);$i++){
            
            $data = array(
                
                'student_name'=>  $finalarray[$i]['student_name'], 
                'mobile_number'=>  $finalarray[$i]['mobile_number'], 
                'alter_mobile_number'=>  $finalarray[$i]['alter_mobile_number'], 
                'selected_course'=> $finalarray[$i]['selected_course'], 
                'admission_status'=>  $finalarray[$i]['admission_status'], 
                'email_id'=>  $finalarray[$i]['email_id'], 
            );
            
            $finalDataArray[] =$data;
        }
        $this->db->insert_batch('manage_admission',$finalDataArray);
        return true;
    }
     

    

    
}

?>